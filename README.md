# TheRealDannyBoy's Dotfiles
Here are my dotfiles, exposed to all the Internet so you can steal stuff.
They are managed with [chezmoi](https://chezmoi.io).
I run FreeBSD on my main machine, though I have been known to hop to various Linux- and BSD-based operating systems from time to time.

## Dependencies
Here I provide a list of the programs that I use and have configuration files for in this repo.
For obvious reasons, I provide the names of the packages for FreeBSD (with origins.)
They may be different for your operating system.

* `shells/zsh`
* `editors/neovim`
* `sysutils/exa`
* `textproc/ripgrep`
* `sysutils/fd`
* `textproc/fzf`
* `x11/kitty`
* `x11-wm/river`
* `x11/fuzzel`
* `x11/swaylock-effects`
* `x11/swayidle`
* `x11/waybar`
* `x11/mako`
* `x11/swaybg`
* `x11/grim`
* `x11/slurp`
* `x11-fonts/nerd-fonts`
* `graphics/intel-backlight`
* `sysutils/chezmoi`

Some configuration files are for applications I don't use anymore.
I kept my dotfiles around just in case.

## Here be monsters!
These dotfiles are in a state of constant flux, and I break things sometimes.
Here, I provide a list of things that you might want to be careful of borrowing from, as well as things I haven't implemented yet.

* **My `zshrc`:** Once upon a time, I broke my Zsh config so badly that I felt it necessary to delete my `zshrc` start over.  Although that was some time ago now, my Zsh is not (yet!) as fancy as some other configurations you can find on the Internet.  Completion is currently case-sensitive, for instance.
* **Locking the screen:** While the screen will lock after inactivity and can be locked manually, I have not yet discovered a method for locking it when it goes to sleep.  This is not best practice.  Stay tuned for improvements.
* **Colorsheme inconsistency:** Some time ago, I moved from the Nord colorscheme to Solarized-Dark.  I may not have properly updated the colorschemes in all of my apps yet.
* **Desktop notifications:** I have only done very basic configuration of Mako.  There are some rough edges.
* **Waybar:** It has recently come to my attention that my Waybar configuration is aggressively FreeBSD-specific.  This will be fixed eventually, once my Linux box works again...
