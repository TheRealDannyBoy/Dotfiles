# Prefix key that is easier to reach
unbind C-b
set -g prefix C-a
bind C-a send-prefix

# vi-like movement around panes
bind 'h' select-pane -L
bind 'j' select-pane -D
bind 'k' select-pane -U
bind 'l' select-pane -R

# vi-like movement in copy mode
set-window-option -g mode-keys vi

# Vim-like visual selection in copy mode
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'y' send -X copy-selection-and-cancel

set -g set-titles on

set -g @resurrect-capture-pane-contents 'on' # Restore pane contents when retoring environment
set -g @resurrect-strategy-nvim 'session' # Restore Neovim sessions when restoring environment
set -g @continuum-restore 'on' # Environment automatically restored on tmux start
set -g @colors-solarized 'dark' # Use dark variant of Solarized colorsheme

# Plugins
set -g @plugin 'tmux-plugins/tpm' # The tmux plugin manager
set -g @plugin 'tmux-plugins/tmux-sensible' # Sensible defaults
set -g @plugin 'tmux-plugins/tmux-resurrect' # Restore tmux environment after system restart
set -g @plugin 'tmux-plugins/tmux-continuum' # Continuous saving of tmux environment
set -g @plugin 'christoomey/vim-tmux-navigator' # Navigate seamlessly between Neovim and tmux
set -g @plugin 'seebi/tmux-colors-solarized' # Solarized colorscheme

# Boilerplate for tpm
if "test ! -d ~/.tmux/plugins/tpm" \
    "run 'git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm && ~/.tmux/plugins/tpm/bin/install_plugins'"

run -b '~/.tmux/plugins/tpm/tpm'
