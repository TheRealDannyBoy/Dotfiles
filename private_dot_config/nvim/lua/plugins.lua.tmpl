return require('packer').startup(function()
  -- Make it all go faster!
  use {
    'lewis6991/impatient.nvim',
    config = function() require('impatient') end,
  }

  -- Packer manages itself
  use {
    'wbthomason/packer.nvim',
    config = function()
      opts = { silent = true }
      vim.keymap.set('n', '<Leader>pp', ':PackerSync<CR>', opts)
      vim.keymap.set('n', '<Leader>ps', ':PackerStatus<CR>', opts)
    end,
  }

  ----------------------------------------------------------------
  --                         Appearance                         --
  ----------------------------------------------------------------
  -- Solarized colors
  use {
    'ishan9299/nvim-solarized-lua',
    config = function()
      vim.cmd('colorscheme solarized')
      vim.g.solarized_termtrans = true
    end,
  }

  -- Pretty startup screen
  use {
    'goolord/alpha-nvim',
    config = function() require'alpha_config' end,
    requires = 'kyazdani42/nvim-web-devicons',
  }

  -- Pretty status line
  use {
    'nvim-lualine/lualine.nvim',
    config = function()
      require'lualine'.setup {
        options = { theme = 'solarized_dark' },
      }
    end,
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }

  -- Smooth scrolling/paging
  use 'psliwka/vim-smoothie'

  -- {{ if not (eq .chezmoi.os "freebsd") }}
  -- Discord Rich Presence support for Neovim
  use 'andweeb/presence.nvim'
  -- {{ end }}

  ----------------------------------------------------------------
  --              Functional Visual Improvements                --
  ----------------------------------------------------------------
  -- Syntax highlighting powered by tree-sitter
  use {
    'nvim-treesitter/nvim-treesitter',
    config = function() 
      require'nvim-treesitter.configs'.setup {
        ensure_installed = "all",  -- Auto-install all grammars
        highlight = {
          enable = true,
          disable = { "tex" },
        },
        rainbow = {
          enable = true,
          extended_mode = true,
          max_file_lines = nil,
        },
      }
    end,
    requires = 'p00f/nvim-ts-rainbow',  -- Rainbow parens
    run = ':TSUpdate',
  }

  -- Show indent levels as vertical lines
  use {
    'Yggdroot/indentLine',
    config = function() 
      vim.g.indentLine_fileTypeExclude = {
        'markdown',
        'tex',
        'text',
        'help',
        'man',
        'alpha',
        'packer',
        'lspconfig',
        'mason',
        'floaterm',
      }
      vim.g.indent_blankline_filetype_exclude = vim.g.indentLine_fileTypeExclude
    end,
    requires = 'lukas-reineke/indent-blankline.nvim', -- Also on blank lines
  }

  -- show marks in the sign column
  use {
    'chentoast/marks.nvim',
    config = function() require'marks'.setup {} end,
  }

  -- highlight convenient targets for 'f', 'F', 't', and 'T'
  use {
    'unblevable/quick-scope',
    config = function()
      vim.g.qs_highlight_on_keys = { 'f', 'F', 't', 'T' }
    end,
  }

  -- highlight comments like TODO, BUG, and HACK
  use {
    'folke/todo-comments.nvim',
    config = function() require("todo-comments").setup() end,
    requires = 'nvim-lua/plenary.nvim',
  }

  -- Show register contents in the sidebar
  use 'junegunn/vim-peekaboo'

  -- Automatic highlighting of other uses of the word under the cursor
  use 'RRethy/vim-illuminate'

  ----------------------------------------------------------------
  --                  Language Server Protocol                  --
  ----------------------------------------------------------------
  -- Package manager for language servers and the like
  use {
    'williamboman/mason.nvim',
    config = function() require('lsp_config') end,
    requires = {
      'neovim/nvim-lspconfig',
      'williamboman/mason-lspconfig.nvim',
    }
  }

  -- Show function signature when you type
  use 'ray-x/lsp_signature.nvim'

  -- Use non-standard clangd LSP features
  use {
    'p00f/clangd_extensions.nvim',
    ft = { 'c', 'cpp' },
  }

  -- Tags management (basically proto-LSP)
  use {
    'ludovicchabant/vim-gutentags',
    -- {{ if eq .chezmoi.os "freebsd" }}
    config = function()
      -- FreeBSD-specific ctags executable
      vim.g.gutentags_ctags_executable = 'uctags'
    end,
    -- {{ end }}
  }

  ----------------------------------------------------------------
  --                   Completion and friends                   --
  ----------------------------------------------------------------
  -- Automatically insert closing quotes and parens
  use 'jiangmiao/auto-pairs'

  -- Easily comment and uncomment lines
  use 'preservim/nerdcommenter'

  -- Easily add, delete, and change "surroundings" (quotes, parens, XML tags, etc.)
  use {
    'tpope/vim-surround',
    requires = 'tpope/vim-repeat' -- Make "." work with surround.vim mappings
  }

  -- Completion with nvim-cmp
  use {
    'hrsh7th/nvim-cmp',
    config = function() require'cmp_config' end,
    requires = {
      'L3MON4D3/LuaSnip',               -- Snippet engine
      'saadparwaiz1/cmp_luasnip',       -- From LuaSnip
      'hrsh7th/cmp-nvim-lsp',           -- From language server
      'onsails/lspkind-nvim',           -- LSP pictograms for completion menu
      'quangnguyen30192/cmp-nvim-tags', -- From tags
      'hrsh7th/cmp-omni',               -- From omnifunc
      'hrsh7th/cmp-nvim-lua',           -- For Neovim Lua buffers
      'jalvesaq/cmp-nvim-r',           -- For Neovim Lua buffers
      'hrsh7th/cmp-buffer',             -- From buffer
      'hrsh7th/cmp-path',               -- From file paths
      'hrsh7th/cmp-cmdline',            -- From Vim commands
    },
  }

  -- Shortcuts for running common Unix commands from Neovim
  use 'tpope/vim-eunuch'


  ----------------------------------------------------------------
  --                     New Functionality                      --
  ----------------------------------------------------------------
  -- The fanciest finder there is
  use {
    'nvim-telescope/telescope.nvim',
    config = function()
      opts = { silent = true }
      vim.keymap.set('n', '<Leader>ff', ':Telescope find_files<CR>', opts)
      vim.keymap.set('n', '<Leader>fg', ':Telescope live_grep<CR>', opts)
      vim.keymap.set('n', '<Leader>fb', ':Telescope buffers<CR>', opts)
      vim.keymap.set('n', '<Leader>fh', ':Telescope help_tags<CR>', opts)
      vim.keymap.set('n', '<Leader>fm', ':Telescope marks<CR>', opts)
      require('telescope').setup({
        extensions = {
          ['ui-select'] = {
            require('telescope.themes').get_dropdown(),
          };
        },
      })
      require('telescope').load_extension('ui-select')
      require('telescope').load_extension('fzf')
    end,
    requires = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope-ui-select.nvim',
      -- {{ if eq .chezmoi.os "freebsd" }}
      { 'nvim-telescope/telescope-fzf-native.nvim', run = gmake },
      -- {{ else if eq .chezmoi.os "windows" }}
      { 'nvim-telescope/telescope-fzf-native.nvim', run = "mingw32-make" },
      -- {{ else }}
      { 'nvim-telescope/telescope-fzf-native.nvim', run = make },
      -- {{ end }}
    },
  }
  
  -- Floating terminal inside Neovim
  use {
    'voldikss/vim-floaterm',
    config = function()
      vim.g.floaterm_width  = 0.9
      vim.g.floaterm_height = 0.9

      vim.g.floaterm_keymap_new     = '<F7>'
      vim.g.floaterm_keymap_next    = '<F8>'
      vim.g.floaterm_keymap_prev    = '<F9>'
      vim.g.floaterm_keymap_toggle  = '<F12>'

      {{- if eq .chezmoi.os "windows" -}}
      vim.g.floaterm_shell="pwsh.exe"
      {{- end }}
    end,
  }

  -------------------------------------------------------------
  --                           Git                           --
  -------------------------------------------------------------
  -- Git wrapper for Vim
  use {
    'tpope/vim-fugitive',
    config = function()
      opts = { silent = true }
      vim.keymap.set('n', '<Leader>gs', ':Git<CR>', opts)
      vim.keymap.set('n', '<Leader>gc', ':Git commit<CR>', opts)
      vim.keymap.set('n', '<Leader>gp', ':Git push<CR>', opts)
      vim.keymap.set('n', '<Leader>gl', ':Git pull<CR>', opts)
      vim.keymap.set('n', '<Leader>gd', ':Gdiffsplit<CR>', opts)
      vim.keymap.set('n', '<Leader>gw', ':Gbrowse<CR>', opts)
    end,
    requires = {
      'shumphrey/fugitive-gitlab.vim',  -- GitLab integration for vim-fugitive
      'tpope/vim-rhubarb',              -- GitHub integration for vim-fugitive
    }
  }

  -- Git signs on left edge
  use {
    'lewis6991/gitsigns.nvim',
    config = function() require('gitsigns').setup() end,
    requires = 'nvim-lua/plenary.nvim',
  }

  -- Git merge conflict resolution
  use {
    'christoomey/vim-conflicted',
    requires = 'tpope/vim-fugitive'
  }

  -------------------------------------------------------------
  --                    Language plugins                     --
  -------------------------------------------------------------
  -- Robust Rust support
  use {
    'simrat39/rust-tools.nvim',
    ft = 'rust',
    config = function() require('rust-tools').setup() end,
    requires = {
      'neovim/nvim-lspconfig',
      'nvim-lua/plenary.nvim',
      'mfussenegger/nvim-dap',
    },
  }

  -- Run Cargo commands from Vim
  use {
    'timonv/vim-cargo',
    ft = 'rust',
    config = function()
      vim.api.nvim_create_autocmd('FileType', {
        pattern = "rust",
        callback = vim.keymap.set('n', '<LocalLeader>r', ':make run<CR>', {silent=true}),
      })
    end,
  }

  -- Completion for crates in Cargo.toml
  use {
    'saecki/crates.nvim',
    tag = 'v0.1.0',
    event = 'BufRead Cargo.toml',
    requires = 'nvim-lua/plenary.nvim',
    config = function()
      require('cmp').setup.buffer {
        sources = { { name = 'crates' } }
      }
      require('crates').setup()
    end,
  }

  -- Robust Go support
  use {
    'ray-x/go.nvim',
    ft = 'go',
    config = function()
      vim.api.nvim_create_autocmd('BufWritePre', {
        pattern = "*.go",
        callback = require('go.format').gofmt(),
      })
      vim.api.nvim_create_autocmd('FileType', {
        pattern = "go",
        callback = vim.keymap.set('n', '<LocalLeader>r', ':GoRun<CR>'),
      })
      require('go').setup()
    end,
    requires = {
      'neovim/nvim-lspconfig',
      'nvim-treesitter/nvim-treesitter',
      'mfussenegger/nvim-dap',
    },
  }

  -- Robust TeX support
  use {
    'lervag/vimtex',
    ft = 'tex',
    config = function()
      vim.o.conceallevel = 2
      vim.g.tex_flavor = "latex"
      vim.g.vimtex_view_method = "zathura"
      vim.api.nvim_create_autocmd( 'FileType', {
        pattern = 'tex',
        callback = function(args)
          vim.treesitter.stop(args.buf)
        end,
      })
    end,
  }

  -- Robust Java support
  use {
    'mfussenegger/nvim-jdtls',
    ft = 'java',
    config = function()
      require('jdtls').start_or_attach {
        cmd = {'/home/lopen/.local/share/jdtls/bin/jdtls'},
        root_dir = vim.fs.dirname(vim.fs.find({'gradlew', '.git', 'mvnw'}, { upward = true })[1]),
      }
    end,
  }

  -- Flutter LSP support
  use {
    'mskelton/flutter.nvim',
    ft = 'dart',
    config = function() require('flutter').setup() end,
    requires = { 'neovim/nvim-lspconfig' },
  }

  -- Run Flutter commands from inside Neovim
  use {
    'thosakwe/vim-flutter',
    ft = 'dart',
    config = function()
      vim.g.flutter_split_height = 12       -- make Flutter log split 12 lines high
      vim.g.flutter_autoscroll = 1          -- autoscroll the Flutter log
      vim.g.flutter_use_last_run_option = 1 -- remember previous run options
      
      -- keybinds
      vim.api.nvim_create_autocmd('FileType', {
        pattern = 'dart',
        callback = function()
          vim.keymap.set('n', '<LocalLeader>r', ':FlutterRun<CR>')
          vim.keymap.set('n', '<LocalLeader>q', ':FlutterQuit<CR>')
          vim.keymap.set('n', '<LocalLeader>vd', ':FlutterVisualDebug<CR>')
        end,
      })
    end,
  }

  -- Robust R support
  use {
    'jalvesaq/nvim-r',
    ft = 'r',
    requires = {
      'nhrsh7th/nvim-cmp',
      'jalvesaq/cmp-nvim-r',
    },
  }

  -- Filetype for the Lilypond music typesetting system
  use 'sersorrel/vim-lilypond'

  -- Filetype for the kitty terminal's config file
  use 'fladson/vim-kitty'
end)

-- vi:ft=lua
