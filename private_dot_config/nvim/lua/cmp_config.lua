local cmp = require('cmp')
local lspkind = require('lspkind')

cmp.setup({
  completion = {
    completeopt = 'menu,menuone,noselect'
  },
  snippet = {
    expand = function(args)
      require'luasnip'.lsp_expand(args.body)
    end
  },
  mapping = {
    ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
    ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
    ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
    ['<C-e>'] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close()
    }),
    ['<CR>'] = cmp.mapping(cmp.mapping.confirm({ select=true }), { 'i', 's' } ),
    ['<Tab'] = cmp.mapping(function(fallback)
      if luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  },
  sources = {
    { name = 'nvim_lsp' },
    { name = 'tags' },
    { name = 'luasnip' },
    { name = 'nvim_lua' },
    { name = 'r' },
    { name = 'omni' },
    { name = 'path' },
    { name = 'buffer' },
  },
  formatting = {
    format = lspkind.cmp_format({
      with_text = true,
      maxwidth = 50
    })
  },
  experimental = {
    ghost_text = true
  }
})

-- Completion for command mode
cmp.setup.cmdline(':', {
  sources = {
    { name = 'cmdline', keyword_length = 2 },
    { name = 'path' }
  }
})

-- Use buffer source for searches
cmp.setup.cmdline('/', {
  sources = {
    { name = 'buffer' }
  }
})
cmp.setup.cmdline('?', {
  sources = {
    { name = 'buffer' }
  }
})
