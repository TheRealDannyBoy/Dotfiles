#!/bin/sh

image() {
    if [ -n "$DISPLAY" ]; then
        file=$1
        w=$2
        h=$3
        x=$4
        y=$5
        kitty +kitten icat --silent --stdin no --transfer-mode memory --place "${w}x${h}@${x}x${y}" "$file" < /dev/null > /dev/tty
        exit 1 # needed for preview to refresh
    else
        chafa "$1" -s "$4x"
    fi
}

CACHE="$HOME/.cache/lf/thumbnail.$(stat -f '%N%n%i%n%T%n%z%n%B%n%m' -- "$(readlink -f "$1")" | sha256 | awk '{ print $1 }')"

case "$(printf "%s\n" "$(readlink -f "$1")" | awk '{ print tolower($0) }')" in
    *.tar*|*.zip)
        tar tf "$1"
        ;;
    *.bmp|*.jpg|*.jpeg|*.png|*.xpm|*.webp|*.tiff|*.gif|*.jfif|*.ico)
        image "$1" "$2" "$3" "$4" "$5"
        ;;
esac
