#!/bin/sh

echo $(sysctl -n hw.acpi.thermal.tz0.temperature | sed 's/C/°C/')
