#!/bin/sh
# Get battery status for Waybar on FreeBSD

life=$(sysctl -n hw.acpi.battery.life)
charging=$(sysctl -n hw.acpi.acline)
time=$(sysctl -n hw.acpi.battery.time)
hours=$(bc -e "$time / 60")
minutes=$(printf "%02d" $(bc -e "$time % 60")) # add leading 0 if <10 mins

if [ "$charging" = 1 ]; then
    if [ "$life" -eq 100 ]; then
        tooltip="Charged"
    else
        tooltip="Charging"
    fi
else
    tooltip="$hours:$minutes remaining"
fi

result=$(cat <<EOF
{
    "text": "freebsd-battery",
    "alt": "$charging",
    "tooltip": "$tooltip",
    "class": "freebsd-battery",
    "percentage": $life
}
EOF
)

echo "$result" | jq --unbuffered --compact-output
