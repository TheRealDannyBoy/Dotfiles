#!/bin/sh

volume=$(mixer -S vol | cut -d: -f1)

if [ "$volume" -eq 0 ]; then
    mixer vol $(< $HOME/.cache/vol_before_mute)
else
    echo "$volume" > $HOME/.cache/vol_before_mute
    mixer vol 0
fi
