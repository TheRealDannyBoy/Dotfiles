#!/bin/sh

brightness=$(backlight | cut -d' ' -f2)

result=$(cat <<EOF
{
    "text": "freebsd-battery",
    "class": "freebsd-battery",
    "percentage": $brightness
}
EOF
)

echo "$result" | jq --unbuffered --compact-output
