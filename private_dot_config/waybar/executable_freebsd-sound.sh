#!/bin/sh

mute="unmuted"
volume=$(mixer -S vol | cut -d: -f2)

if [ "$volume" -eq 0 ]; then
    mute="muted"
fi

result=$(cat <<EOF
{
    "text": "freebsd-volume",
    "alt": "$mute",
    "percentage": $volume
}
EOF
)

echo "$result" | jq --unbuffered --compact-output
