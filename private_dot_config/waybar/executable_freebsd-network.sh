#!/bin/sh
# Get network status for a lagg device set up to
# fail over from Ethernet to Wi-Fi on FreeBSD

# change to match your computer
lagg_if='lagg0'
ether_if='em0'
wifi_if='wlan0'

netstatus="$(ifconfig "$lagg_if")"
addresses="$(echo "$netstatus" | grep -w inet | cut -d' ' -f2)"
active_if="$(echo "$netstatus" | grep ACTIVE | cut -d' ' -f2)"

case $active_if in
    $ether_if)
        text='Ethernet'
        alt='ethernet'
        ;;
    $wifi_if)
        text="$(ifconfig $wifi_if | grep ssid | cut -d' ' -f2)"
        alt="wi-fi"
        ;;
    *)
        text='No Internet connection'
        alt='none'
        ;;
esac

result=$(cat <<EOF
{
    "text": "$text",
    "alt": "$alt",
    "tooltip": "$addresses",
    "class": "freebsd-network"
}
EOF
)

echo "$result" | jq --unbuffered --compact-output
