-- Base
import XMonad
import qualified Data.Map as M
import qualified XMonad.StackSet as W

-- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)

-- IO
import System.IO

-- Layouts
import XMonad.Layout.ResizableTile

-- Layouts modifiers
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Spacing
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

-- Prompt
import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.XMonad

-- Util
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig

-- terminal to launch
myTerminal      = "alacritty"

-- Left Win key as Mod
myModMask       = "mod4Mask"

-- Solarized colors
colorBackground = "#002b36"
colorForeground = "#839496"
colorYellow     = "#b58900"
colorOrange     = "#cb4b16"
colorRed        = "#dc322f"
colorMagenta    = "#d33682"
colorViolet     = "#6c71c4"
colorBlue       = "#268bd2"
colorCyan       = "#2aa198"
colorGreen      = "#859900"

-- fonts
myFont          = "xft:Source Code Pro:regular:size=9:antialias=true:hinting=true"

-- show a window count
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myManageHook = composeAll
    -- float what needs floating
    [ className =? "Xmessage" --> doFloat
    , className =? "Steam" --> doFloat
    , className =? "Peek" --> doFloat
    , className =? "yubioath-desktop" --> doFloat
    , className =? "Yubioath Desktop" --> doFloat
    , (className =? "Firefox" <&&> appName =? "Dialog") --> doFloat
    , (className =? "Firefox" <&&> title =? "Picture-in-Picture") --> doFloat
    -- fullscreen
    , isFullscreen --> doFullFloat
    -- accomodate xmobar
    , manageDocks
    , manageHook def
    ]

-- customize XMonad prompt appearances
myXPConfig = def { searchPredicate = fuzzyMatch
                 , sorter          = fuzzySort
                 }

-- Layouts
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- no gaps if there's only one window
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

tall    = renamed [Replace "tall"]
          $ mySpacing 8
          $ ResizableTall 1 (3/100) (1/2) []
magnify = renamed [Replace "magnify"]
          $ magnifier
          $ mySpacing 8
          $ ResizableTall 1 (3/100) (1/2) []
monocle = renamed [Replace "monocle"]
          $ Full

myShowWNameTheme = def { swn_font       = "xft:Liberation Sans:bold:size=60"
                       , swn_fade       = 1.0
                       , swn_bgcolor    = "#839496"
                       , swn_color      = "#002b36"
                       }

myLayoutHook    = avoidStruts $ showWName' myShowWNameTheme
                $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
                where
                    myDefaultLayout =     tall
                                      ||| magnify
                                      ||| monocle

-- keyboard shortcuts
myKeys =
    -- I prefer rofi over dmenu
    [ ("M-p",                     spawn "rofi -show drun")

    -- pause/unpause my notifications
    , ("M-n",                     spawn "notify-send DUNST_COMMAND_TOGGLE")

    -- layouts
    , ("M-<Tab>",                 sendMessage NextLayout)
    , ("M-<Space>",               sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)
    , ("M-S-<Space>",             sendMessage ToggleStruts)
    , ("M-S-n",                   sendMessage $ MT.Toggle NOBORDERS)

    -- take screenshots with Flameshot
    , ("<Print>",                 spawn "flameshot gui -p ~/pictures/screenshots")
    , ("C-<Print>",               spawn "flameshot gui -d 3000 -p ~/pictures/screenshots")
    , ("S-<Print>",               spawn "flameshot screen -p ~/pictures/screenshots")
    , ("C-S-<Print>",             spawn "flameshot screen -c")

    -- control volume
    , ("<XF86AudioRaiseVolume>",  spawn "change-volume 5%+")
    , ("<XF86AudioLowerVolume>",  spawn "change-volume 5%-")
    , ("<XF86AudioMute>",         spawn "change-volume toggle")

    -- control media player
    , ("<XF86AudioPlay>",         spawn "playerctl play-pause")
    , ("<XF86AudioNext>",         spawn "playerctl next")
    , ("<XF86AudioPrev>",         spawn "playerctl previous")

    -- brightness control
    , ("<XF86MonBrightnessUp>",   spawn "change-brightness -A 5")
    , ("<XF86MonBrightnessDown>", spawn "change-brightness -D 5")
    ]

main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ docks $ ewmh def
        { handleEventHook = docksEventHook <+> fullscreenEventHook
        , layoutHook = myLayoutHook
        , logHook = dynamicLogWithPP xmobarPP
            { ppOutput = hPutStrLn xmproc
            , ppTitle = xmobarColor colorForeground "" . shorten 50
            , ppCurrent = xmobarColor colorGreen "" . wrap "[" "]"
            , ppVisible = xmobarColor colorGreen ""
            , ppHidden = xmobarColor colorBlue "" . wrap "*" ""
            , ppHiddenNoWindows = xmobarColor colorViolet ""
            , ppSep = "<fc=#93a1a1> <fn=1>|</fn> </fc>"
            , ppExtras = [windowCount]
            , ppOrder = \(ws:l:t:ex) -> [ws,l]++ex++[t]
            }
        , terminal = myTerminal
        , modMask = mod4Mask
        , normalBorderColor = colorBackground
        , focusedBorderColor = colorForeground
        , manageHook = myManageHook
        } `additionalKeysP` myKeys
