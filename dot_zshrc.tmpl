# Zinit boilerplate [[[
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Important annexes/plugins that can't be loaded with Turbo
zinit light-mode for \
    zinit-zsh/z-a-patch-dl \
    zinit-zsh/z-a-as-monitor \
    zinit-zsh/z-a-bin-gem-node
# ]]]
# Zsh configuration [[[
zstyle :compinstall filename $HOME/.zshrc

setopt NO_BEEP          # DON'T BEEP AT ME
setopt NO_NOTIFY        # update me on the status of background jobs only at new prompts
setopt NO_EXTENDED_GLOB # Treat ~, ^, and # like normal characters
setopt NOMATCH          # If a pattern doesn't match, return an error
setopt AUTO_CD          # If given a directory as a command, cd there

# history [[[
HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=5000
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
# ]]]
# ]]]
# aliases [[[
alias h='fc -l'
alias j='jobs -l'
alias m="$PAGER"
alias za='zathura'
alias mkexec='chmod +x'
alias rm='rm -Iv'   # prompt for removal if removing 4+ files, and be verbose
alias df='df -h'
alias du='du -ch'
alias diff='diff -urpN'
alias info='info --vi-keys'
alias icat='kitty +kitten icat'
alias clip='kitty +kitten clipboard'
alias ykssh='ssh-add -s /usr/local/lib/opensc-pkcs11.so'
# colored manpages
man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    GROFF_NO_SGR=1 \
    command man "$@"
}

# view Markdown as HTML
mdv() {
    pandoc -i $1 | w3m -T text/html
}

# do a DuckDuckGo search (supports bangs)
duck() {
    w3m "https://lite.duckduckgo.com/lite/?q=$(echo $@ | sed "s/[[:blank:]]/+/g")"
}
# ls [[[
alias ls='exa --icons'
alias ll='exa --long --git --icons --group-directories-first'
alias la='exa --long --git --icons --group-directories-first --all'
alias l='exa --long --git --icons --group-directories-first --all --classify'
alias tree='exa --tree'
# ]]]
# nvim [[[
alias vim='nvim'
alias vi='nvim'
alias v='nvim'
alias vr='nvim -R'
# ]]]
# chezmoi [[[
alias cm='chezmoi'
alias cma='chezmoi apply'
alias cmd='chezmoi diff'
alias cmad='chezmoi add'
alias cmg='chezmoi git'

cmc() {
    until [ "$BW_SESSION" ]; do
        export BW_SESSION="$(bw unlock --raw)"
    done

    chezmoi cd
}

cme() {
    until [ "$BW_SESSION" ]; do
        export BW_SESSION="$(bw unlock --raw)"
    done

    chezmoi edit $@
}
# ]]]
# cargo [[[
alias co='cargo'
alias cr='cargo run'
alias ccl='cargo clean'
alias ccy='cargo clippy'
alias cb='cargo build --release'
alias ct='cargo test'
alias cad='cargo add'
alias ci='cargo install'
alias ciu='cargo install-update -a'
alias cfi='cargo fix'
alias cfm='cargo fmt'
alias cfe='cargo fetch'
alias cpa='cargo package'
alias cpl='cargo publish'
alias cs='cargo search'
alias cfa='cargo fmt; cargo fix --allow-dirty --allow-staged'
# ]]]
# pkgng/ports (FreeBSD only) [[[
{{ if eq .chezmoi.os "freebsd" -}}
alias pki='sudo pkg install'
alias pkia='sudo pkg install --automatic'
alias pkif='sudo pkg install --force'
alias pkim='sudo pkg install --ignore-missing'
alias pkin='sudo pkg install --dry-run'
alias pkir='sudo pkg install --recursive'

alias pkd='sudo pkg delete'
alias pkdn='pkg delete --dry-run'
alias pkdr='pkg delete --recursive'

alias pkar='sudo pkg autoremove'

alias pku='sudo pkg upgrade'
alias pkun='pkg upgrade --dry-run'

alias pks='pkg search'
alias pksc='pkg search --comment'
alias pksd='pkg search --description'
alias pksdo='pkg search --depends-on'
alias pksf='pkg search --full'
alias pkso='pkg search --origins'
alias pksp='pkg search --prefix'
alias pkss='pkg search --size'
alias pksi='pkg isearch'

alias pkf='pkg info'
alias pkfa='pkg info --all'
alias pkfm='pkg info --pkg-message'
alias pkfc='pkg info --comment'
alias pkfr='pkg info --required-by'
alias pkfl='pkg info --list-files'
alias pkfs='pkg info --size'
alias pkfd='pkg info --dependencies'
alias pkfo='pkg info --origin'
alias pkfp='pkg info --prefix'
alias pkfi='pkg iinfo'
alias pkh='pkg show'
alias pkl='pkg list'
alias pkad='pkg all-depends'
alias pkpd='pkg provided-depends'
alias pkrd='pkg required-depends'
alias pkw='pkg which'
alias pko='pkg options'
alias pkal='pkg alias'

pmk() {
    port="$1"
    shift
    make -C "/usr/ports/${port}" $@
}
{{- end }}
# ]]]
# btrfs filesystem usage commands (Linux only) [[[
{{ if eq .chezmoi.os "linux" -}}
alias bu='sudo btrfs filesystem usage'
alias bdu='sudo btrfs filesystem du'
alias bdf='sudo btrfs filesystem df'
{{- end }}
# ]]]
# stolen from oh-my-zsh [[[
zinit wait lucid for OMZP::git
zinit wait lucid for OMZP::golang
zinit wait lucid for OMZP::rust
{{ if eq .chezmoi.os "linux" -}}
zinit wait lucid for OMZP::systemd
zinit wait lucid for OMZP::suse
zinit wait lucid for OMZP::dnf
{{- end }}
# ]]]
# ]]]
# set prompt
export PS1="%B%F{magenta}[%F{red}%n%F{yellow}@%F{green}%m %F{cyan}%c%F{blue}]%# %b%f"
export RPS1="%(0?..%B%F{red}(%?%)%b%f)"
autoload -Uz compinit
compinit

{{ if eq .chezmoi.os "linux" -}}
# support for command-not-found
. /etc/zsh_command_not_found
{{- end }}

# zsh-autosuggestions settings [[[
autosuggest_bindings() {
    bindkey '^[[Z' autosuggest-accept # Shift-tab
    bindkey '^ ' autosuggest-execute # Ctrl-space
}
# ]]]
# zsh-fast-autocompletion settings [[[
substring_bindings() {
    bindkey '^[[A' history-substring-search-up # up arrow
    bindkey '^[[B' history-substring-search-down # down arrow
    bindkey '^P' history-substring-search-up # Ctrl-P
    bindkey '^N' history-substring-search-down # Ctrl-N
    bindkey -M vicmd 'k' history-substring-search-up # k in vi mode
    bindkey -M vicmd 'j' history-substring-search-down # j in vi mode
}
# ]]]
# have a fortune, but not if this is a login shell--you already got one :)
if [[ ! "$-" =~ ".*l.*" ]] && type fortune >/dev/null; then
    if type randcowsay >/dev/null; then
        fortune -s | randcowsay
    elif type cowsay >/dev/null; then
        fortune -s | cowsay
    else
        echo
        fortune -s
    fi
    echo
fi

# plugins that must reload the prompt [[[
zinit wait'!' lucid nocd for \
    atload'vim-mode-precmd; vim-mode-cursor-init-hook' \
        softmoth/zsh-vim-mode
# ]]]
# make zsh friendlier [[[
zinit wait lucid for \
    atinit'zicompinit; zicdreplay' \
        zdharma/fast-syntax-highlighting \
    atload'autosuggest_bindings; _zsh_autosuggest_start' \
        zsh-users/zsh-autosuggestions \
    atload'substring_bindings' \
        zsh-users/zsh-history-substring-search \
    hlissner/zsh-autopair
# ]]]
# new features [[[
zinit wait lucid for \
    wait'type sudo >/dev/null' OMZP::sudo \
    arzzen/calc.plugin.zsh
# ]]]

# vim: ft=zsh foldmethod=marker foldmarker=[[[,]]]
